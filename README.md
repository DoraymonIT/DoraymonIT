### Hi there 👋

<!-- 
**DoraymonIT/DoraymonIT** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started: -->

- 🔭 I’m currently working on BEREXIA Conseil Consulting as a Front End Developer using ANGULAR and its tools ,... .
- 🌱 I’m currently learning FIGMA to make better UI/UX and also Deep Diving in Angular .
- 👯 I’m looking to collaborate on some Open Sources .
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: bendrimou@gmail.com
- 😄 Pronouns: 
- ⚡ Fun fact: I code then I exist then I solve problems then I make life easier for our clients  .

